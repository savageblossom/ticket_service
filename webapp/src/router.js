import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import TicketDetails from "@/views/TicketDetails.vue";
import Admin from "@/views/Admin/index.vue";
import SessionDetails from "@/views/Admin/SessionDetails.vue";
import Auth from "@/views/Admin/Auth.vue";
import store from "./store";

Vue.use(VueRouter);

const routes = [
  { path: "/", component: Home },
  {
    name: "TicketDetails",
    path: "/ticket/:id",
    component: TicketDetails,
  },
  {
    path: "/admin",
    component: Admin,
    meta: { requiresAuth: true },
  },
  {
    name: "CreateSession",
    path: "/admin/new_session",
    component: SessionDetails,
    meta: { requiresAuth: true },
  },
  {
    name: "Auth",
    path: "/admin/auth",
    component: Auth,
  },
  {
    name: "UpdateSession",
    path: "/admin/:sessionId",
    component: SessionDetails,
    meta: { requiresAuth: true },
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach(async (to, from, next) => {
  await store.dispatch("verify_token");
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const is_admin_logged = store.getters.is_admin_logged;
    if (!is_admin_logged) {
      next({
        path: "/admin/auth",
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
