import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    sessions_map: null,
    current_session: null,
    is_loading: false,
    is_admin_logged: false,
    token: "",
    current_ticket: null,
  },
  mutations: {
    set_sessions(state, sessions) {
      state.sessions_map = sessions;
    },
    set_current_session(state, session) {
      state.current_session = session;
    },
    set_loading(state, truthy) {
      state.is_loading = truthy;
    },
    set_admin_logged(state, truthy) {
      state.is_admin_logged = truthy;
    },
    set_token(state, token) {
      state.token = token;
    },
    set_current_ticket(state, ticket) {
      state.current_ticket = ticket;
    },
  },
  actions: {
    async fetch_sessions({ commit }) {
      commit("set_loading", true);
      const fetchData = await Vue.$http.get("http://localhost:3000/session");

      commit("set_sessions", fetchData.data);
      commit("set_loading", false);
    },
    async fetch_session_by_id({ commit }, { sessionId }) {
      const fetchData = await Vue.$http.get(
        `http://localhost:3000/session/${sessionId}`
      );

      commit("set_current_session", fetchData.data);
    },
    async update_session(_, session) {
      return await Vue.$http.put(
        `http://localhost:3000/session/${session._id}`,
        session
      );
    },
    async create_session(_, session) {
      return await Vue.$http.post(`http://localhost:3000/session`, session);
    },
    async remove_session(_, { sessionId }) {
      return await Vue.$http.delete(
        `http://localhost:3000/session/${sessionId}`
      );
    },
    async verify_token({ commit, state }) {
      const access_token = localStorage.getItem("token") || state.token;

      try {
        await Vue.$http.post(`http://localhost:3000/auth/verify`, {
          access_token,
        });
        commit("set_admin_logged", true);
        commit("set_token", access_token);
        return;
      } catch (_) {
        commit("set_admin_logged", false);
        commit("set_token", "");
        return;
      }
    },
    async fetch_ticket({ commit }, params) {
      const fetchData = await Vue.$http.get(`http://localhost:3000/ticket`, {
        params,
      });

      commit("set_current_ticket", fetchData.data[0]);
    },
    async buy_ticket(_, ticketBody) {
      return await Vue.$http.post(`http://localhost:3000/ticket`, ticketBody);
    },
  },
  getters: {
    sessions(state) {
      return state.sessions_map;
    },
    current_session(state) {
      return state.current_session;
    },
    is_loading(state) {
      return state.is_loading;
    },
    is_admin_logged(state) {
      return state.is_admin_logged;
    },
    token(state) {
      return state.token;
    },
    current_ticket(state) {
      return state.current_ticket;
    },
  },
});

export default store;
