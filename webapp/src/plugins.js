import Vue from "vue";

import "font-awesome/css/font-awesome.min.css";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import Vuelidate from "vuelidate";

import setupAxios from "./axios";

export default function () {
  setupAxios();

  Vue.use(Buefy, {
    defaultIconPack: "fa",
  });
  Vue.use(Vuelidate);
}
