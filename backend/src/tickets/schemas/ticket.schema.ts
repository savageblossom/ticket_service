import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { Session } from 'src/sessions/schemas/session.schema';

export type TicketDocument = Ticket & Document;

@Schema()
export class Ticket {
  @Prop({ type: SchemaTypes.ObjectId, ref: Session })
  session_id: string;

  @Prop()
  phone: string;

  @Prop()
  date: Date;

  @Prop()
  cost: number;

  @Prop()
  seat: number;
}

export const TicketSchema = SchemaFactory.createForClass(Ticket);
