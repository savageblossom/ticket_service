import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Mongoose, Query, Types } from 'mongoose';
import { SessionsService } from 'src/sessions/sessions.service';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { Ticket, TicketDocument } from './schemas/ticket.schema';

@Injectable()
export class TicketsService {
  constructor(
    @InjectModel(Ticket.name) private ticketModel: Model<TicketDocument>,
    private sessionService: SessionsService,
  ) {}

  async getAll(query?: { sessionId: string; seat: number }): Promise<Ticket[]> {
    const aggregationQuery = [
      {
        $match: {},
      },
      {
        $lookup: {
          from: 'sessions',
          let: { sid: '$session_id' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$_id', { $toObjectId: '$$sid' }],
                },
              },
            },
          ],
          as: 'session',
        },
      },
      {
        $unwind: '$session',
      },
    ];

    if (query) {
      aggregationQuery[0].$match = {
        seat: {
          $eq: Number(query.seat),
        },
        session_id: {
          $eq: Types.ObjectId(query.sessionId),
        },
      };
    }

    return this.ticketModel.aggregate(aggregationQuery);
  }

  async getById(id: string): Promise<Ticket> {
    return this.ticketModel.findById(id);
  }

  async create(ticketDto: CreateTicketDto): Promise<Ticket> {
    const newTicket = new this.ticketModel(ticketDto);

    const targetSession = await this.sessionService.getById(
      newTicket.session_id,
    );

    const { max_seats, occupied_seats } = targetSession;

    if (occupied_seats.length >= max_seats) {
      throw new HttpException(
        'All seats are occupied',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    const isSeatAlreadyTaken = targetSession.occupied_seats.some(
      (occupiedSeat) => occupiedSeat === newTicket.seat,
    );

    if (isSeatAlreadyTaken) {
      throw new HttpException(
        'That seat already taken',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    occupied_seats.push(newTicket.seat);

    await this.sessionService.updateSeatsList(
      occupied_seats,
      newTicket.session_id,
    );

    return newTicket.save();
  }

  async remove(id: string): Promise<Ticket> {
    const ticketToRemove = await this.ticketModel.findById(id);
    const targetSession = await this.sessionService.getById(
      ticketToRemove.session_id,
    );

    let { occupied_seats } = targetSession;

    occupied_seats = occupied_seats.filter(
      (occupiedSeat) => ticketToRemove.seat !== occupiedSeat,
    );

    await this.sessionService.updateSeatsList(
      occupied_seats,
      ticketToRemove.session_id,
    );

    return this.ticketModel.findByIdAndRemove(id);
  }

  async update(id: string, ticketDto: UpdateTicketDto): Promise<Ticket> {
    return this.ticketModel.findByIdAndUpdate(id, ticketDto, { new: true });
  }
}
