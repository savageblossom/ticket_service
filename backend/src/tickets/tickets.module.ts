import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TicketsService } from './tickets.service';
import { TicketsController } from './tickets.controller';
import { Ticket, TicketSchema } from './schemas/ticket.schema';
import { SessionsModule } from 'src/sessions/sessions.module';

@Module({
  providers: [TicketsService],
  controllers: [TicketsController],
  imports: [
    MongooseModule.forFeature([{ name: Ticket.name, schema: TicketSchema }]),
    SessionsModule,
  ],
})
export class TicketsModule {}
