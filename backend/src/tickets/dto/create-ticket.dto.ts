export class CreateTicketDto {
  readonly session_id: string;
  readonly phone: string;
  readonly date: Date;
  readonly cost: number;
  readonly seat: number;
}
