import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { TicketsService } from './tickets.service';
import { Ticket } from './schemas/ticket.schema';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('ticket')
export class TicketsController {
  constructor(private readonly ticketsService: TicketsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  getAll(
    @Query('sessionId') sessionId,
    @Query('seat') seat,
  ): Promise<Ticket[]> {
    if (sessionId && seat) {
      return this.ticketsService.getAll({ sessionId, seat });
    }

    return this.ticketsService.getAll();
  }

  @Get(':id')
  getOne(@Param('id') id: string): Promise<Ticket> {
    return this.ticketsService.getById(id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Cache-Control', 'none')
  create(@Body() createTicketDto: CreateTicketDto): Promise<Ticket> {
    return this.ticketsService.create(createTicketDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<Ticket> {
    return this.ticketsService.remove(id);
  }

  @Put(':id')
  update(
    @Body() updateTicketDto: UpdateTicketDto,
    @Param('id') id: string,
  ): Promise<Ticket> {
    return this.ticketsService.update(id, updateTicketDto);
  }
}
