import {
  Body,
  Controller,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth/auth.service';
import { LocalAuthGuard } from './auth/local-auth.guard';

@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @HttpCode(HttpStatus.OK)
  @Post('auth/verify')
  async verifyToken(@Body() req) {
    try {
      await this.authService.verifyToken(req.access_token);
      return;
    } catch (e) {
      throw new HttpException('Bad token', HttpStatus.UNAUTHORIZED);
    }
  }
}
