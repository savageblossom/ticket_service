import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { CreateSessionDto } from './dto/create-session.dto';
import { UpdateSessionDto } from './dto/update-session.dto';
import { SessionsService } from './sessions.service';
import { Session } from './schemas/session.schema';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('session')
export class SessionsController {
  constructor(private readonly sessionsService: SessionsService) {}

  @Get()
  getAll(): Promise<Session[]> {
    return this.sessionsService.getAll();
  }

  @Get(':id')
  getOne(@Param('id') id: string): Promise<Session> {
    return this.sessionsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Cache-Control', 'none')
  create(@Body() createSessionDto: CreateSessionDto): Promise<Session> {
    return this.sessionsService.create(createSessionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string): Promise<Session> {
    return this.sessionsService.remove(id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(
    @Body() updateSessionDto: UpdateSessionDto,
    @Param('id') id: string,
  ): Promise<Session> {
    return this.sessionsService.update(id, updateSessionDto);
  }
}
