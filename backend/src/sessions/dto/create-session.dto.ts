export class CreateSessionDto {
  readonly title: string;
  readonly description: string;
  readonly date: Date;
  readonly max_seats: number;
  readonly occupied_seats: number[];
  readonly cost: number;
}
