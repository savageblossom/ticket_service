import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Session, SessionDocument } from './schemas/session.schema';
import { CreateSessionDto } from './dto/create-session.dto';
import { UpdateSessionDto } from './dto/update-session.dto';

@Injectable()
export class SessionsService {
  constructor(
    @InjectModel(Session.name) private sessionsModel: Model<SessionDocument>,
  ) {}

  async getAll(): Promise<Session[]> {
    return this.sessionsModel.find().exec();
  }

  async getById(id: string): Promise<Session> {
    return this.sessionsModel.findById(id);
  }

  async create(sessionDto: CreateSessionDto): Promise<Session> {
    const newProduct = new this.sessionsModel(sessionDto);
    return newProduct.save();
  }

  async remove(id: string): Promise<Session> {
    return this.sessionsModel.findByIdAndRemove(id);
  }

  async update(id: string, sessionDto: UpdateSessionDto): Promise<Session> {
    return this.sessionsModel.findByIdAndUpdate(id, sessionDto, { new: true });
  }

  async updateSeatsList(
    seatList: number[],
    sessionId: string,
  ): Promise<Session> {
    return this.sessionsModel.findByIdAndUpdate(
      sessionId,
      {
        $set: {
          occupied_seats: seatList,
        },
      },
      {
        new: true,
      },
    );
  }
}
