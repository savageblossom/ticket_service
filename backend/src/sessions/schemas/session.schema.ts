import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SessionDocument = Session & Document;

@Schema()
export class Session {
  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop()
  date: Date;

  @Prop()
  max_seats: number;

  @Prop()
  occupied_seats: number[];

  @Prop()
  cost: number;
}

export const SessionSchema = SchemaFactory.createForClass(Session);
